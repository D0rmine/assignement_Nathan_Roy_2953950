﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    public grumbleAMP amp;
    public int ID;
    private static Music instance = null;
    public static Music Instance
    {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this && ID == instance.ID) {
            Destroy(this.gameObject);
            return;
        }
        else if (instance != null && instance != this && ID != instance.ID)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        amp.PlaySong(0, 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
