﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimesUpScreen : MonoBehaviour {

    Image img;
	// Use this for initialization
	void Start () {
        img = this.GetComponent<Image>();
        img.enabled = false;
	}

    public void Show()
    {
        img.enabled = true;
    }	
}
