﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour {

    public GameObject Player;
    private Vector3 SavePlayerPos;

	// Use this for initialization
	void Start () {
        SavePlayerPos = Player.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //look from distance
            transform.Translate(Player.transform.position - SavePlayerPos);
            Vector3 posMouse = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
            posMouse.z = transform.position.z;
            Vector3 dir = posMouse - transform.position;
            if (Player != null && Player.GetComponent<SpriteRenderer>().isVisible)
                transform.Translate(dir*2*Time.deltaTime);
        }
        else
        {
            if (Player != null)
                transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, transform.position.z);
        }
        if (Player != null)
            SavePlayerPos = Player.transform.position;
    }
}
