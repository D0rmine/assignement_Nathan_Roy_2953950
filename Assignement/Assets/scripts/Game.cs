﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {

    public bool Loose;
    public bool win;
    public string scene;
    public WiningScreen winScreen;
    public Text Timer;
    float TimeLeft;
    public float TimeMax;
    public TimesUpScreen TimeUpScreen;


    // Use this for initialization
    void Start () {
        Loose = false;
        win = false;
        TimeLeft = TimeMax;
    }

    public bool IsFinished()
    {
        if (win || Loose)
            return true;
        return false;
    }

    public void NextAfterWining()
    {
        if (win)
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            win = false;
        }
    }

    // Update is called once per frame
    void Update () {
        TimeLeft -= Time.deltaTime;
        if (TimeLeft <= 0)
        {
            Timer.text = "00.00";
            Loose = true;
            TimeUpScreen.Show();
        }
        else
            Timer.text = TimeLeft.ToString("00.00");
        if (Loose)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(scene, LoadSceneMode.Single);
                Loose = false;
            }
        }
        else if (win)
        {
            winScreen.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
                win = false;
            }
        }
	}
}
