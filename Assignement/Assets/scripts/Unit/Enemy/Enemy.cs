﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy : Unit {

    public GameObject Player;
    protected Vector3 LastPositionPlayerSeen;
    protected  Seeker seeker;
    protected int time;
    protected Rigidbody2D body;
    public Path path;
    public Collider2D hearing;
    public float speed = 2;
    public List<PatrolPoint> patrolPoint;
    protected int currentPatrolPoint;
    public enum StatusEnemy { IDLE, PATROLE, CHASE, ATTACK };
    public StatusEnemy status;
    LayerMask mask;

    public float nextWaypointDistance = 3;
    protected int currentWaypoint = 0;

    virtual protected void Start () {
        time = 0;
        mask = ~(1 << LayerMask.NameToLayer("Enemy"));
        currentPatrolPoint = 0;
        body = GetComponent<Rigidbody2D>();
        seeker = GetComponent<Seeker>();
        status = StatusEnemy.PATROLE;
    }

    // Update is called once per frame
    override protected void Update () {
        base.Update();
        if (LastPositionPlayerSeen == Vector3.zero)
        {
            status = StatusEnemy.PATROLE;
        }
        else
        {
            status = StatusEnemy.CHASE;
        }
        if (Player != null)
        {
            if (hearing.IsTouching(Player.GetComponent<Collider2D>()))
               Hearing(Player.gameObject);
        }
        if (status == StatusEnemy.PATROLE && path == null)
        {
            if (patrolPoint.Count > 0 && currentPatrolPoint < patrolPoint.Count)
            {
                seeker.CancelCurrentPathRequest();
                currentWaypoint = 0;
                seeker.StartPath(transform.position, patrolPoint[currentPatrolPoint].position, OnPathComplete);
            }
        }
        if (status != StatusEnemy.ATTACK)
            Path();
    }


    //if the player is around look around see if you see him
    virtual protected void Hearing(GameObject player) {
       RaycastHit2D hit = Physics2D.Raycast(transform.position + transform.up, transform.up, Mathf.Infinity, mask);
       RaycastHit2D hit2 = Physics2D.Raycast(transform.position + Vector3.Normalize(player.transform.position - transform.position), player.transform.position - transform.position, Mathf.Infinity, mask);

        if (hit.collider.gameObject == player)
        {
            status = StatusEnemy.ATTACK;
            body.velocity = Vector2.zero;
            Attack();
        }
        else if (hit2.collider.gameObject == player)
        {
            status = StatusEnemy.ATTACK;
            body.velocity = Vector2.zero;
            Rotate();
        }
        DedectThePlayer(player);
    }

    //empty method for the heritage
    virtual protected void Attack()
    {

    }

    //empty method for the heritage
    virtual protected void Rotate()
    {

    }

    virtual protected void Path()
    {
        if (path == null)
        {
            //We have no path to move after yet
            currentWaypoint = 0;
            if (status == StatusEnemy.PATROLE)
            {
                if (currentPatrolPoint >= patrolPoint.Count)
                    currentPatrolPoint = 0;
                else
                    currentPatrolPoint++;
            }
            if (status == StatusEnemy.CHASE)
                LastPositionPlayerSeen = Vector3.zero;
            status = StatusEnemy.PATROLE;
            body.velocity = Vector2.zero;
            body.freezeRotation = true;
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            body.velocity = Vector2.zero;
            path = null;
            return;
        }

        Vector3 dir = new Vector3(path.vectorPath[currentWaypoint].x - transform.position.x, path.vectorPath[currentWaypoint].y - transform.position.y, 0).normalized;
        transform.up = new Vector2(path.vectorPath[currentWaypoint].x - transform.position.x, path.vectorPath[currentWaypoint].y - transform.position.y);
        body.velocity = new Vector2((path.vectorPath[currentWaypoint].x - transform.position.x) * speed, (path.vectorPath[currentWaypoint].y - transform.position.y) * speed);
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }

    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            //Reset the waypoint counter
            currentWaypoint = 0;
        }
    }

    //if you see the player place the last position you see him at his position
    protected void DedectThePlayer(GameObject player)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.Normalize(player.transform.position - transform.position), player.transform.position - transform.position, Mathf.Infinity, mask);
        if (hit.collider.gameObject == player)
        {
            status = StatusEnemy.ATTACK;
            LastPositionPlayerSeen = player.transform.position;
            seeker.CancelCurrentPathRequest();
            currentWaypoint = 0;
            seeker.StartPath(transform.position, LastPositionPlayerSeen, OnPathComplete);
        }
    }
}
