﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class EnemyInfanteryControler : Enemy {

    public AudioClip shootSound;

    private float volLowRange = 0.1f;
    private float volHighRange = .4f;

    override protected void Start()
    {
        base.Start();
        attack_speed = 0;
    }

    protected override void Update()
    {
        if (dead)
            return;
        base.Update();
    }

    override protected void Rotate()
    {
        base.Rotate();
        transform.up = Player.transform.position - transform.position;
    }

    override protected void Attack()
    {
        base.Attack();
        if (attack_speed == 0)
            {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(shootSound, vol);
            bullet.transform.position = this.transform.position + this.transform.up * 3 / 2;
                bullet.GetComponent<BulletControler>().velocity = transform.up * bullet_speed;
            bullet.GetComponent<BulletControler>().tag_attack = "Player";
            Instantiate(bullet);
            }
        if (attack_speed >= 0.2f)
            {
                attack_speed = 0;
            }
        else
            attack_speed += Time.deltaTime;
    }
}
