﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControler : Unit {

    private int time;
    public Collider2D triger;
    public Camera cam;
    public float speed;
    public int enrgieMax;
    public Image DeathScreen;
    public int energie;
    public Rigidbody2D body;
    public Armor armor;
    public Item ItemIWalkIn;
    public List<Gadget> gadgets;
    public int current_gadget;
    public Image GadgetImage;
    public Text GadgetName;
    public Text GadgetAmo;
    public Image ArmorImage;
    public Text ArmorName;
    public Sprite Default;
    public Menu menu;
    bool menuEnable;
    protected Game game;
    public AudioClip shootSound;

    private float volLowRange = 0.1f;
    private float volHighRange = .4f;

    // Use this for initialization
    void Start () {
        game = GetComponentInParent<Game>();
        menuEnable = false;
        current_gadget = 0;
        ItemIWalkIn = null;
        armor = null;
        DeathScreen.fillAmount = 0;
        attack_speed = 0;
        Hp = HpMax;
        energie = enrgieMax;
        body = GetComponent<Rigidbody2D>();
        time = 0;
    }

    // Death function
    protected override void Death()
    {
        DeathScreen.fillAmount = 1;
        game.Loose = true;
        base.Death();
    }
    Collider2D[] col;

    //update
    protected override void Update() {
        if (game.IsFinished())
            return;
        base.Update();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //enable or disable the menu
            if (menuEnable)
            {
                menu.gameObject.SetActive(false);
                menuEnable = false;
            }
            else
            {
                menu.gameObject.SetActive(true);
                menuEnable = true;
            }
        }
        if (menuEnable)
            return;
        body.velocity = Vector3.zero;
        Move();
        if (time >= 10)
            time = 0;
        else
            time++;
        lookAt();
        Shoot();
        EnergyRecovery();
        if (Input.GetKeyDown(KeyCode.A) && gadgets.Count > 0)
        {
            //change gadget
            current_gadget++;
            if (current_gadget >= gadgets.Count)
            {
                current_gadget = 0;
            }
            GadgetImage.sprite = gadgets[current_gadget].ItemSprite;
            GadgetName.text = gadgets[current_gadget].Name;
            GadgetAmo.text = ""+gadgets[current_gadget].amo;
        }
        if (Input.GetMouseButtonDown(1) && gadgets.Count > 0)
        {
            //use gadget
            gadgets[current_gadget].UseGadgetCapacity();
            if (gadgets.Count > 0)
            {
                if (current_gadget >= gadgets.Count)
                    current_gadget -= 1;
                GadgetImage.sprite = gadgets[current_gadget].ItemSprite;
                GadgetName.text = gadgets[current_gadget].Name;
                GadgetAmo.text = "" + gadgets[current_gadget].amo;
            }
            else
            {
                GadgetImage.sprite = Default;
                GadgetName.text = " ";
                GadgetAmo.text = "";
            }
        }
        if (Input.GetKeyDown(KeyCode.T) && ItemIWalkIn != null)
        {
            //take an item on the ground
            take();
            if (armor != null)
            {
                ArmorImage.sprite = armor.ItemSprite;
                ArmorName.text = armor.Name;
            }
            if (gadgets.Count > 0)
            {
                GadgetImage.sprite = gadgets[current_gadget].ItemSprite;
                GadgetName.text = gadgets[current_gadget].Name;
                GadgetAmo.text = "" + gadgets[current_gadget].amo;
            }
        }
        if (Input.GetKeyDown(KeyCode.Space) && armor != null)
        {
            armor.UseArmorCapacity();
        }
    }

    //look if your are on an item
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Item>() != null)
           ItemIWalkIn = other.gameObject.GetComponent<Item>();
        if (other.gameObject.GetComponent<EndOfLevel>() != null)
        {
            game.win = true;
            body.velocity = Vector2.zero;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Item>() != null)
            ItemIWalkIn = null;
    }

    //take an item below you
    void take()
    {
        ItemIWalkIn.PlayerTake(this);
        ItemIWalkIn.gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    //permanente Energy recovery
    void EnergyRecovery()
    {
        if (energie < enrgieMax && time == 0)
            energie++;
    }

    //look at the mouse
    void lookAt()
    {
        Vector3 mousse2DPosition = Input.mousePosition;

        RaycastHit hit;

        Vector3 mouseClickDirection = cam.GetComponent<Camera>().ScreenToWorldPoint(mousse2DPosition);
        Ray ray = cam.ScreenPointToRay(mousse2DPosition);
        if (Input.mousePresent && Physics.Raycast(ray, out hit))
        {
            transform.up = new Vector2(hit.point.x - transform.position.x, hit.point.y - transform.position.y);
        }
    }

    void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (attack_speed == 0)
            {
                float vol = Random.Range(volLowRange, volHighRange);
                source.PlayOneShot(shootSound, vol);
                bullet.transform.position = this.transform.position + this.transform.up * 3/2;
                bullet.GetComponent<BulletControler>().velocity = transform.up * bullet_speed;
                bullet.GetComponent<BulletControler>().tag_attack = "Enemy";
                Instantiate(bullet);
            }
            if (attack_speed == 3)
            {
                attack_speed = 0;
            }
            else
                attack_speed++;
        }
        else
        {
            attack_speed = 0;
        }
    }

    public void Move()
    {
        double move = speed * 1;
        if (Input.GetKey(KeyCode.Z))
        {
            body.velocity = new Vector2(body.velocity.x + 0, body.velocity.y + (float)move);
        }
        if (Input.GetKey(KeyCode.S))
        {
            body.velocity = new Vector2(body.velocity.x + 0, body.velocity.y + -(float)move);
        }
        if (Input.GetKey(KeyCode.D))
        {
            body.velocity = new Vector2(body.velocity.x + (float)move, body.velocity.y + 0);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            body.velocity = new Vector2(body.velocity.x + -(float)move, body.velocity.y + 0);
        }
    }
}
