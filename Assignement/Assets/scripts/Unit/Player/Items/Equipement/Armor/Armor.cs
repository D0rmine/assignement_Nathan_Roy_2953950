﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Equipment {

    override protected void Start()
    {
        base.Start();
    }

    public override void PlayerTake(PlayerControler pl)
    {
        base.PlayerTake(pl);
        pl.armor = this;
    }

    public virtual void UseArmorCapacity()
    {
    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
    }
}
