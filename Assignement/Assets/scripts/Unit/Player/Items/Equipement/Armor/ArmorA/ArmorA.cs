﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorA : Armor
{
    public AudioClip Teleportation;

    public AudioSource source;
    private float volLowRange = 0.1f;
    private float volHighRange = .4f;
    // Use this for initialization
    override protected void Start()
    {
        base.Start();
        Name = "ArmorA";
    }

    //teleportation
    public override void UseArmorCapacity()
    {
        base.UseArmorCapacity();

        if (player.energie >= 20)
        {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(Teleportation, vol);
            player.energie -= 20;
            player.transform.position = new Vector3(player.transform.position.x + player.body.velocity.x / 6, player.transform.position.y + player.body.velocity.y / 6, player.transform.position.z);
        }
    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
    }
}
