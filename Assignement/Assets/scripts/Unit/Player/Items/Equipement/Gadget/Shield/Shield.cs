﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : Gadget {

    public GameObject shield;
    public AudioClip ShieldSound;

    public AudioSource source;
    private float volLowRange = 0.4f;
    private float volHighRange = .8f;

    override protected void Start()
    {
        base.Start();
        Name = "Shield";
    }

    //create a shield
    public override void UseGadgetCapacity()
    {
        base.UseGadgetCapacity();
        if (player.energie >= 20)
        {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(ShieldSound, vol);
            amo -= 1;
            Vector3 mousse2DPosition = Input.mousePosition;
            RaycastHit hit;

            Vector3 mouseClickDirection = player.cam.ScreenToWorldPoint(mousse2DPosition);
            Ray ray = player.cam.ScreenPointToRay(mousse2DPosition);
            if (Physics.Raycast(ray, out hit))
            {
                shield.transform.position = new Vector3(hit.point.x, hit.point.y, -1);
                shield.transform.up = new Vector2(player.transform.position.x - shield.transform.position.x, player.transform.position.y - shield.transform.position.y);
                Instantiate(shield);
                player.energie -= 20;
            }
        }
    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
    }
}
