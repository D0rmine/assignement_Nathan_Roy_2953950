﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Gadget
{
    public GameObject grenade;

    // Use this for initialization
    override protected void Start()
    {
        base.Start();
        Name = "Grenade";
    }

    public override void UseGadgetCapacity()
    {
        base.UseGadgetCapacity();
        amo -= 1;
        grenade.transform.position = player.transform.position + player.transform.up * 3 / 2;
        grenade.GetComponent<GrenadeControler>().velocity = player.transform.up * 20;
        grenade.GetComponent<GrenadeControler>().tag_attack = "Enemy";
        Instantiate(grenade);
    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
    }
}
