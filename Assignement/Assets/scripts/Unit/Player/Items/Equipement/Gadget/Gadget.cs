﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gadget : Equipment {

    public int amo;

	// Use this for initialization
	override protected void Start () {
        base.Start();
	}

    //taking by the player
    public override void PlayerTake(PlayerControler pl)
    {
        base.PlayerTake(pl);
        pl.gadgets.Add(this);
    }

    //what the player use when he use the gadget
    public virtual void UseGadgetCapacity()
    {
        if (amo <= 1)
        {
            player.gadgets.Remove(this);
            Destroy(gameObject);
            Destroy(this);
        }
    }

    // Update is called once per frame
    override protected void Update () {
        base.Update();
        if (amo <= 0)
        {
            player.gadgets.Remove(this);
            Destroy(gameObject);
            Destroy(this);
        }
	}
}
