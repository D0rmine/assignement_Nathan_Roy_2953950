﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : Item {

    public Sprite ItemSprite;
    public string Name; 

	// Use this for initialization
	override protected void Start () {
        base.Start();
	}

    //set the type of the equipment
    protected override void Set()
    {
        base.Set();
        type = TypeItem.EQUIPMENT;
    }

    //taking by the player
    public override void PlayerTake(PlayerControler pl)
    {
        base.PlayerTake(pl);
    }

    // Update is called once per frame
    override protected void Update () {
        base.Update();
	}
}
