﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public enum TypeItem { EQUIPMENT, WAEPON, REGENERATOR };
    public TypeItem type;
    public PlayerControler player;
    public bool collected;
    public AudioClip PickUpSound;

    public AudioSource sourcePickUp;

    // Use this for initialization
    virtual protected void Start () {
        collected = false;
	}

    virtual protected void Set()
    {

    }

    virtual public void PlayerTake(PlayerControler pl)
    {
        if (collected == false) {
            sourcePickUp.PlayOneShot(PickUpSound, 1);
            collected = true;
            player = pl;
        }
    }

    // Update is called once per frame
    virtual protected void Update () {
		if (collected == true)
        {
            if (player != null)
                transform.position = player.transform.position;
        }
	}

}
