﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {
    public int HpMax;
    public int Hp;
    public GameObject bullet;
    public int bullet_speed;
    protected float attack_speed;
    public AudioClip DeathSond;

    public AudioSource source;
    public AudioSource sourceDeath;
    protected bool dead = false;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	virtual protected void Update () {
        if (Hp <= 0)
        {
            Death();
        }
    }

    virtual protected void Death()
    {
        if (!dead)
        {
            dead = true;
            sourceDeath.PlayOneShot(DeathSond, 1);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(this.gameObject, 1);
        }
    }
}
