﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeControler : MonoBehaviour
{
    private Animator animator;
    public GameObject explosion;
    public string tag_attack;
    public Vector2 velocity;
    Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>() as Animator;
        body.velocity = velocity;
    }


    void Update()
    {
        AnimatorStateInfo asi = animator.GetCurrentAnimatorStateInfo(0);

        //animation of rolling grenade
        if (!asi.IsName("Roll") || asi.normalizedTime >= 3)
        {
            explosion.transform.position = transform.position;
            Instantiate(explosion);
            Destroy(gameObject);
        }
    }

    //explode if contact with an enemy
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == tag_attack)
        {
            explosion.transform.position = transform.position;
            Instantiate(explosion);
            Destroy(gameObject);
        }
    }
}