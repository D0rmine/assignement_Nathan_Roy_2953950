﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>() as Animator;
    }


    void Update()
    {
        AnimatorStateInfo asi = animator.GetCurrentAnimatorStateInfo(0);

        if (!asi.IsName("Explosion") || asi.normalizedTime >= 1)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
        {
            if (!other.isTrigger)
                other.gameObject.GetComponent<Unit>().Hp -= 100;
        }
    }
}
